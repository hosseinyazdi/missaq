from django.contrib import admin
from .models import Profile
from program.models import Message_reciving,Message
from program.utils import sendSMS

from userena.admin import UserenaAdmin
from django.contrib.auth.models import User

def sendmessage(self, request, queryset):
    """
    Exports the selected rows using file_format.
    """
    sendsms = request.POST.get('sendsms')
    sendemail = request.POST.get('sendemail')
    title = request.POST.get('messageTitle', '')
    textcontent = request.POST.get('message text', '')
    message = Message()
    message.subject = title
    message.status='profile'
    message.content = textcontent
     # = .profile


    message.sender=Profile.objects.filter(user=request.user).first()
    message.save()
    for item in queryset:
        message_reciving = Message_reciving()
        message_reciving.message_id = message.id
        message_reciving.profile_id = item.id
        message_reciving.save()
    inbox_filter = request.POST.get('sendwithin')
    if inbox_filter == 'inbox':
        message.sendInbox = True
        message.save()

    inbox_filter = request.POST.get('sendemail')
    if inbox_filter == 'email':
        message.sendEmail = True
        message.save()
        to_email = queryset.values_list(
            'user__email', flat=True)
        if title and textcontent:
            from django.core.mail import EmailMessage
            message2 = EmailMessage(title, textcontent, bcc=to_email)
            message2.send()
    inbox_filter = request.POST.get('sendsms')
    if inbox_filter == 'sms':
        message.sendSms = True
        message.save()
        to = queryset.values_list(
            'cellPhone', flat=True)
        sendSMS(to, textcontent)
sendmessage.short_description = ('فرستادن پیام')
def registrations_to_excel(self, request, queryset):
    status_list = []
    for j in queryset:
        profile__user__first_name = j.user.first_name
        profile__user__last_name = j.user.last_name
        if j.gender:
            profile__gender = 'مرد'
        else:
            profile__gender = 'زن'
        profile__adress_type = j.get_adress_type_display()
        profile__studentnumber=j.studentNumber
        profile__enteracetyear=j.entranceYear()
        profile__yearold=j.yaeroldenter()
        profile__user__email = j.user.email
        profile__cellPhone = j.cellPhone
        if j.interstToCoaoprat:
            Registration_feedback = True
        else:
            Registration_feedback=False
        profile__coaoparatdes=j.coaopratDes

        object = (
            profile__user__first_name,
            profile__user__last_name,
            profile__gender,
            profile__studentnumber,
            profile__enteracetyear,
            profile__yearold,
            profile__adress_type,
            profile__user__email,
            profile__cellPhone,
            Registration_feedback,
            profile__coaoparatdes
        )
        status_list.append(object)
    from program.utils import export_user_xls
    return export_user_xls(status_list)
registrations_to_excel.short_description = ('اکسل گرفتن')
def delete_selected(modeladmin, request, queryset):
    for obj in queryset:
        obj.delete()
delete_selected.short_description = 'حذف موارد انتخاب شده'
class ProfileAdmin(admin.ModelAdmin):
    list_display = ['__str__','gender','email','cellPhone','people_type','studentNumber','personwith','interstToCoaoprat','entranceYear','yaeroldenter']
    list_filter = ['gender','people_type','level_type','personwith','interstToCoaoprat']

    search_fields = ('user__first_name', 'user__last_name', 'user__username',)
    def email(self, obj):
        return obj.user.email
    email.short_description = 'ایمیل'

    actions = [delete_selected,sendmessage,registrations_to_excel]

admin.site.unregister(Profile)
admin.site.register(Profile, ProfileAdmin)

