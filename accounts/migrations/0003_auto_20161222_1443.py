# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-12-22 11:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20161222_1434'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='level_Separation',
        ),
        migrations.AddField(
            model_name='profile',
            name='level_type',
            field=models.CharField(blank=True, choices=[('bs', 'کارشناسی'), ('ms', 'کارشناسی ارشد'), ('phd', 'دکترا'), ('none', ' بدون مقطع')], max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='peopletype',
            field=models.CharField(choices=[('student', 'دانشجو'), ('master', 'استاد'), ('staff', 'کارمند'), ('other', 'سایر')], max_length=200),
        ),
    ]
