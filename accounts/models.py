# Encoding: utf-8
from django.db import models
from datetime import datetime
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from userena.models import UserenaBaseProfile
from django.utils.translation import ugettext_lazy as _


# Create your models here.
class Profile(UserenaBaseProfile):
    user = models.OneToOneField(User,
                                on_delete=models.CASCADE,
                                unique=True,
                                verbose_name=_('user'),
                                related_name='my_profile')
    entranceDate = models.DateTimeField(max_length=20, default=datetime.now)
    melliCode = models.CharField(max_length=10, verbose_name='کد ملی')
    gender = models.BooleanField(default=1,verbose_name='مرد')
    interstToCoaoprat = models.BooleanField(default=0, verbose_name='علاقه به همکاری ')
    coaopratDes=models.CharField(max_length=10, null=True, blank=True,verbose_name='توضسحات علاقه مندی ')
    major = models.CharField(max_length=20, null=True, blank=True,verbose_name='رشته ')
    ADRESS_TYPE_TEHRANI = 'TEHRANI'
    ADRESS_TYPE_DORMITORY = 'DORMITORY'

    adress_type_choices = (
        (ADRESS_TYPE_TEHRANI, 'تهرانی'),
        (ADRESS_TYPE_DORMITORY, 'خوابگاهی'),
    )
    adress_type = models.CharField(max_length=200, choices=adress_type_choices,verbose_name='وضعیت سکونت ')
    # address = models.CharField(max_length=400)
    # shenasname = models.CharField(max_length=11)
    credit = models.IntegerField(null=True, blank=True,verbose_name='اعتبار ')
    studentNumber = models.CharField(max_length=20, null=True, blank=True,verbose_name='شماره دانشجویی ')
    fatherName = models.CharField(max_length=200, null=True, blank=True,verbose_name='نام پدر')
    cellPhone = models.CharField(max_length=11,verbose_name='شماره تلفن ')
    # emergencyPhone = models.CharField(max_length=20, null=True, blank=True)
    # conscriptionDesc = models.CharField(max_length=200, null=True, blank=True)
    # deActivated = models.BooleanField(default=False)
    # birthYear = models.IntegerField(null=True,verbose_name='سال تولد ')
    # birthMonth = models.IntegerField(null=True)
    # birthDay = models.IntegerField(null=True)
    PEOPLE_TYPE_SHARIF_STUDENT = 'sharif student'
    PEOPLE_TYPE_SHARIF_GRADUATED = 'sharif graduated'
    PEOPLE_TYPE_SHARIF_MASTER = 'sharif master'
    PEOPLE_TYPE_SHARIF_EMPLOYED = 'sharif employed'
    PEOPLE_TYPE_OTHER = 'other'
    people_type_choices = (
        (PEOPLE_TYPE_SHARIF_STUDENT, 'دانشجو شریف'),
        (PEOPLE_TYPE_SHARIF_GRADUATED, 'فارغ التحصیل شریف'),
        (PEOPLE_TYPE_SHARIF_MASTER, 'استاد شریف'),
        (PEOPLE_TYPE_SHARIF_EMPLOYED, 'کارمند شریف'),
        (PEOPLE_TYPE_OTHER, 'سایر'),
    )

    people_type = models.CharField(max_length=200, choices=people_type_choices,verbose_name='نوع عضویت ')
    LEVEL_TYPE_B_S = 'BS'
    LEVEL_TYPE_M_S = 'MS'
    LEVEL_TYPE_P_H_D = 'PHD'

    level_type_choices = (
        (LEVEL_TYPE_B_S, 'کارشناسی'),
        (LEVEL_TYPE_M_S, 'کارشناسی ارشد'),
        (LEVEL_TYPE_P_H_D, 'دکترا'),
    )
    level_type = models.CharField(null=True, blank=True, max_length=200, choices=level_type_choices,verbose_name='مقطع ')
    def entranceYear(self):
        if  self.studentNumber:
            a =str( self.studentNumber)
            return int(a[0:2])
        else:
            return False
    entranceYear.short_description = ('سال ورود')
    # entranceYear = models.IntegerField(null=True, blank=True,verbose_name='سال ورودی ')
    personwith=models.BooleanField(default=0,verbose_name='شخص همراه ')
    personwithname = models.CharField(max_length=20,null=True, blank=True,verbose_name='نام شخص همراه ')
    personwithnamemilicode = models.CharField(max_length=11,null=True, blank=True,verbose_name='کدملی شخص همراه ')
    def registered_on_last(self):
        from program.models import Registration
        from program.utils import getLastProgram
        return Registration.objects.filter(profile=self).filter(program=getLastProgram()).exclude(
            status=Registration.STATUS_REMOVED).first()
    def yaeroldenter(self):
        from program import jalali
        import datetime
        if self.studentNumber:
            a = str(self.studentNumber)
            y= int(a[0:2])
        else:
            y= False
        if y:
            shamsidate=jalali.Gregorian(datetime.datetime.now().year, datetime.datetime.now().month, datetime.datetime.now().day)
            if shamsidate.persian_month < 7 :
                return shamsidate.persian_year - y - 1300
            else:
                return shamsidate.persian_year - y - 1299
        else:
            return False
    yaeroldenter.short_description = ('چندمین سال')

    def delete(self, *args, **kwargs):
        self.user.delete()
        return super(self.__class__, self).delete(*args, **kwargs)


    def __str__(self):
        return self.user.first_name + ' ' + self.user.last_name

    class Meta:
        verbose_name = 'حساب کاربری'
        verbose_name_plural = 'حسابهای کاربری'
