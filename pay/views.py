from django.shortcuts import render
from  program.models import Registration, Program, Profile
from django.views.decorators.csrf import csrf_exempt
from .models import Payment,Expense
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

# Create your views here.
def start_pay(request, registration_id):
    reg = Registration.objects.filter(id=registration_id).first()
    if not reg or request.user != reg.profile.user:
        return HttpResponseRedirect('/error')

    amount = int(request.POST.get('amount', 10000))
    payment = Payment.create(profile=reg, amount=amount)
    return render(request, "post.html", {'payment': payment})


@csrf_exempt
def payment_callback(request):
    refId = request.POST.get("RefId")
    saleReferenceId = request.POST.get("SaleReferenceId")
    saleOrderId = request.POST.get("SaleOrderId")
    resCode = request.POST.get("ResCode")

    if resCode != '0':
        return render(request, 'result.html', {'token': {'success': False, 'verify_rescode': 'Incomplete Transaction'}})

    payment = Payment.objects.filter(refId=refId).first()

    payment.verify(saleReferenceId, saleOrderId)
    if payment.success:
        numofpayment = payment.registration.numberOfPayments
        a = numofpayment + 1
        payment.registration.numberOfPayments = a
        payment.registration.save()
    return render(request, 'result.html', {'payment': payment})


def charity(request):

    if request.method == 'GET':
        all_expenses=Expense.objects.filter(is_open=True)
        return render(request, 'charity.html', {'all_expenses':all_expenses})
    else:
        expense=Expense.objects.get(id=request.POST.get('expense',1))
        amount = int(request.POST.get('amount',10000))

        payment = Payment.create(amount=amount,expense=expense)
        return render(request, "post.html", {'payment': payment})
@login_required
def credit(request):
    profile = Profile.objects.filter(user=request.user).first()
    # return render(request, 'credit.html', {})
    if request.method == 'GET':
        all_expenses = profile.credit
        return render(request, 'credit.html', {'all_expenses': all_expenses})
    else:
        # expense = Expense.objects.get(id=request.POST.get('expense', 1))
        amount = int(request.POST.get('amount', 10000))

        payment = Payment.create(amount=amount, profile=profile, expense=None)
        return render(request, "post.html", {'payment': payment})