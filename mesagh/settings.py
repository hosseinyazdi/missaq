"""
Django settings for azzahra project.

Generated by 'django-admin startproject' using Django 1.9.8.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.9/ref/settings/
"""
from django.conf import settings
import os
# import pymysql
# import pymysql.cursor

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '!%k2v4k!9*(%@ft_by9gh02%#n@b^vfiljin^3h+n!9&qj7_ds'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'accounts',
    'program',
    # 'password_reset',
    'django.contrib.sites',
    'userena',
    'guardian',
    'easy_thumbnails',
    'pay',
    'import_export',
    'django_jalali',


]
# db = MySQLdb.connect(host="127.0.0.1", user="root", passwd="123456", db="mesagh")
# cursor = db.cursor()
# conn= pymysql.connect(host='127.0.0.1',user='root',password='123456',db='mesagh',charset='utf8mb4',cursorclass=pymysql.cursors.DictCursor)
# a=conn.cursor()
# sql='CREATE TABLE `users` (`id` int(11) NOT NULL AUTO_INCREMENT,`email` varchar(255) NOT NULL,`password` varchar(255) NOT NULL,PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;'
# a.execute(sql)
# cursor.execute("ALTER DATABASE %s CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci'" % "mesagh")
#
# sql = "SELECT DISTINCT(table_name) FROM information_schema.columns WHERE table_schema = '%s'" % "mesagh"
# cursor.execute(sql)
#
# results = cursor.fetchall()
# for row in results:
#     sql = "ALTER TABLE %s convert to character set DEFAULT COLLATE DEFAULT" % (row[0])
#     cursor.execute(sql)
# db.close()
from . import localdata
DATABASES = localdata.DATABASES
MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'mesagh.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
            ],
        },
    },
]

WSGI_APPLICATION = 'mesagh.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

AUTHENTICATION_BACKENDS = (
    'userena.backends.UserenaAuthenticationBackend',
    'guardian.backends.ObjectPermissionBackend',
    'django.contrib.auth.backends.ModelBackend',
)
ANONYMOUS_USER_ID = -1

AUTH_PROFILE_MODULE = 'accounts.Profile'
# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = False
EMAIL_HOST = 'misaaq.sharif.ir'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'info@misaaq.sharif.ir'
EMAIL_HOST_PASSWORD = 'm7A;x5L1'

DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
SERVER_EMAIL = EMAIL_HOST_USER
# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'fa-ir'

from django.utils.translation import ugettext_lazy as _
LANGUAGES = (
    ('en', _('English')),
    ('fa', _('Farsi')),
)

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)
TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/
# admin.site.unregister(Profile)
# admin.site.register(User, MyUserAdmin)
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)
SITE_ID=1
from .local import MEDIA_ROOT_LOCAL,DATABASES_LOCAL
# MEDIA_ROOT = os.path.join(os.path.abspath(''),'app/static/')
MEDIA_ROOT=MEDIA_ROOT_LOCAL
# DATABASES=DATABASES_LOCAL
USERENA_HTML_EMAIL=True
ALLOW_UNICODE_SLUGS = True
USERENA_REDIRECT_ON_SIGNOUT = getattr(settings,
                                      'USERENA_REDIRECT_ON_SIGNOUT',
                                      '/accounts/signin')
USERENA_SIGNIN_REDIRECT_URL = getattr(settings,
                                      'USERENA_SIGNIN_REDIRECT_URL',
                                      '/program')
# USERENA_MUGSHOT_PATH = getattr(settings,
#                                'USERENA_MUGSHOT_PATH',
#                                'mugshots/')

LOGIN_URL='/accounts/signin/'
USERENA_MUGSHOT_SIZE = 120*120