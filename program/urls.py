from django.conf.urls import url
from django.contrib import admin

urlpatterns = [

    url(r'^$', 'program.views.my_programs'),
    url(r'^registration/(?P<registration_id>\d+)/', 'program.views.registration'),
    # url(r'^documents/(?P<management_id>\d+)', 'program.views.documentation'),
    # url(r'print/(?P<management_id>\d+)/(?P<profile_id>\d+)', 'program.views.only_print'),
]
