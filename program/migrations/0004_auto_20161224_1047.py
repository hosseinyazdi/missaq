# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-12-24 07:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('program', '0003_auto_20161222_1152'),
    ]

    operations = [
        migrations.AlterField(
            model_name='capacity',
            name='level_Separation',
            field=models.CharField(blank=True, choices=[('BS', 'کارشناسی'), ('MS', 'کارشناسی ارشد'), ('PHD', 'دکترا'), ('none', 'بدون مقطع')], max_length=200, null=True, verbose_name='تفکیک مقطع'),
        ),
        migrations.AlterField(
            model_name='capacity',
            name='peopletype',
            field=models.CharField(choices=[('sharif student', 'دانشجو شریف'), ('sharif graduated', 'فارغ التحصیل شریف'), ('sharif master', 'استاد شریف'), ('sharif employed', 'کارمند شریف'), ('other', 'سایر')], max_length=200),
        ),
    ]
