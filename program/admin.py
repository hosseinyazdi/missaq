from .models import Program, Registration, Message, Message_reciving
from django.contrib import admin
from .models import Capacity
from import_export import fields
# from program.overridedadmin.import_export.admin import ImportExportActionModelAdmin
# from import_export.admin import ImportExportActionModelAdmin
from django.forms import Textarea
from django.db import models
from accounts.models import Profile

from .utils import export_registrations_xls
def make_published(modeladmin, request, queryset):
    queryset.update(status='reserved')
make_published.short_description = "status=reserved"


class Program_Admin(admin.ModelAdmin):
    list_display = ['title', 'year',  'number_of_register', 'certain_or_came','capecity','remain','withcapecity','withremain','withprice']
    list_filter = ['title']

    # inlines = [ManagementInline]





admin.site.register(Program, Program_Admin)

def make_reserved(modeladmin, request, queryset):
    selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
    queryset.update(status='reserved')
make_reserved.short_description = "status=reserved"
def make_certain(modeladmin, request, queryset):
    queryset.update(status='certain')
make_certain.short_description = "status=certain"
def make_givenup(modeladmin, request, queryset):
    for item in queryset:
        item.reserved()
    queryset.update(status='given up')
make_givenup.short_description = "status=given up"
def make_removed(modeladmin, request, queryset):
    queryset.update(status='removed')
make_removed.short_description = "status=removed"
def make_came(modeladmin, request, queryset):
    queryset.update(status='came')
make_came.short_description = "status=came"
def make_notcame(modeladmin, request, queryset):
    queryset.update(status='not came')
make_notcame.short_description = "status=not came"




def sendmessage(self, request, queryset):
    """
    Exports the selected rows using file_format.
    """
    sendsms = request.POST.get('sendsms')
    sendemail = request.POST.get('sendemail')
    sendwithin= request.POST.get('sendwithin')
    title = request.POST.get('messageTitle', '')
    textcontent = request.POST.get('message text', '')
    message = Message()
    message.subject = title
    message.status = 'registration'
    message.content = textcontent
     # = .profile


    message.sender=Profile.objects.filter(user=request.user).first()
    message.save()
    for item in queryset:
        message_reciving = Message_reciving()
        message_reciving.message_id = message.id
        message_reciving.registration_id = item.id
        message_reciving.save()
    inbox_filter = request.POST.get('sendwithin')
    if inbox_filter == 'inbox':
        message.sendInbox = True
        message.save()

    inbox_filter = request.POST.get('sendemail')
    if inbox_filter == 'email':
        message.sendEmail = True
        message.save()
        to_email = queryset.values_list(
            'profile__user__email', flat=True)
        if title and textcontent:
            from django.core.mail import EmailMessage
            message2 = EmailMessage(title, textcontent, bcc=to_email)
            message2.send()
    inbox_filter = request.POST.get('sendsms')
    if inbox_filter == 'sms':
        message.sendSms = True
        message.save()
        to = queryset.values_list(
            'profile__cellPhone', flat=True)
        from .utils import sendSMS

        sendSMS(to, textcontent)
sendmessage.short_description = ('فرستادن پیام')
def registrations_to_excel(self, request, queryset):
    status_list = []
    for j in queryset:
        status = j.status
        profile__user__first_name = j.profile.user.first_name
        profile__user__last_name = j.profile.user.last_name
        if j.profile.gender:
            profile__gender='مرد'
        else:
            profile__gender='زن'
        profile__adress_type=j.profile.get_adress_type_display()
        profile__user__email = j.profile.user.email
        profile__cellPhone = j.profile.cellPhone
        profile_mellicode = j.profile.user.username
        Registration_feedback = j.feedBack


        object = (
                  status,
                  profile__user__first_name,
                  profile__user__last_name,
                  profile__gender,
                  profile__adress_type,
                  profile__user__email,
                  profile__cellPhone,
                  profile_mellicode,
                  Registration_feedback
                  )
        status_list.append(object)
    return export_registrations_xls(status_list)
registrations_to_excel.short_description = ('اکسل گرفتن')
class Registration_Admin(admin.ModelAdmin):
    list_display = ['profile','capacity',  'program', 'registrationDate', 'status','get','personwith']
    formfield_overrides = {
        models.TextField: {'widget': Textarea(
            attrs={'rows': 1,
                   'cols': 40,
                   'style': 'height: 1em;'})},
    }
    def get(self, obj):
        return obj.profile.get_adress_type_display()
    get.short_description = ('نوع سکونت')
    list_filter = [ 'status','program','capacity','profile']
    search_fields = ['profile__user__username', 'program__title']
    actions = [make_reserved,make_certain,make_givenup,make_removed,make_notcame,make_came,sendmessage,registrations_to_excel]
    export_order = ('id', 'profile', 'capacity', 'program')
    myfield = fields.Field(column_name='myfield')

    class Meta:
        model = Registration

        fields = ('id', 'profile', 'capacity', 'program')


admin.site.register(Registration, Registration_Admin)

class Capacity_Admin(admin.ModelAdmin):
    list_display = ['program', 'peopletype', 'enteranceyear_Separation','level_Separation','gender_Separation','capecity','price','remain']
    # form = BookForm
#

admin.site.register(Capacity, Capacity_Admin)

class Message_reciving_Admin(admin.ModelAdmin):
    list_display = ['id', 'message', 'registration','profile']


admin.site.register(Message_reciving, Message_reciving_Admin)


class Message_Admin(admin.ModelAdmin):
    list_display = ['id', 'sender','status', 'subject', 'content', 'sendEmail', 'sendSms', 'sendInbox', 'messageSendDate']


admin.site.register(Message, Message_Admin)
