from django.core.mail import EmailMessage
from django.shortcuts import render
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from .models import Registration, Profile, Message_reciving, Capacity
# Create your views here.


@login_required


@login_required
def registration(request, registration_id):
    myregister = Registration.objects.filter(id=registration_id).first()
    if not myregister or request.user != myregister.profile.user:
        return HttpResponseRedirect('/error')
    if request.method == 'GET':

        massageinbox = Message_reciving.objects.filter(registration_id=registration_id)
        pricings = myregister.capacity.price
        return render(request, 'registration_details.html',
                      {'massageinbox': massageinbox,
                       'pricings': pricings,
                       'myregister': myregister,
                       })
    else:
        if 'givenup_btn' in request.POST:
            myregister.reserved()

        if 'feedback_btn' in request.POST:
            registration_feedback = request.POST.get("feedback", '')
            myregister.feedBack = registration_feedback
            myregister.save()
        return HttpResponseRedirect('/program/registration/' + str(registration_id))


@login_required
def my_programs(request):
    profile = request.user.my_profile
    from .utils import checkstatus,getLastProgram
    last_program=getLastProgram()
    withstatus,status,price,capid = checkstatus(profile)


    if request.method == 'GET':
        if profile.people_type == Profile.PEOPLE_TYPE_SHARIF_STUDENT:
            if not profile.studentNumber or not profile.adress_type or not profile.level_type:
                messages.add_message(request, messages.INFO,
                                     'لطفا پروفایل خود را تکمیل فرمایید  ')
                return render(request, 'my_programs.html')
        else:
            if not profile.adress_type :
                messages.add_message(request, messages.INFO,
                                     'لطفا پروفایل خود را تکمیل فرمایید  ')

                return render(request, 'my_programs.html')
        my_registrations = Registration.objects.filter(profile=profile).exclude(status=Registration.STATUS_REMOVED).order_by('-id')
        a = profile.user.my_profile.registered_on_last


        return render(request, 'my_programs.html',
                      {'regs': my_registrations, 'last_program': last_program, 'fmypricing': price,
                       'status': status, 'user': profile.user, 'a': a, 'withstatus': withstatus})
    else:
        if status == 'valid and available':
            k = request.POST.get('personwith', '')
            credits = profile.credit
            u = credits - price
            if k:
                if last_program.withprice + price > credits:
                    my_registrations = Registration.objects.filter(profile=profile).exclude(
                        status=Registration.STATUS_REMOVED)
                    a = profile.user.my_profile.registered_on_last
                    messages.add_message(request, messages.INFO,
                                         'جمع کل هزینه همراه و قیمت شما بیشتر از اعتبار شما است  ')
                    return render(request, 'my_programs.html',
                                  {'regs': my_registrations, 'last_program': last_program, 'fmypricing': price,
                                   'status': status, 'user': profile.user, 'a': a, 'withstatus': withstatus})
                else:
                    u = credits - price - last_program.withprice
            t = Profile.objects.filter(id=profile.id).first()
            t.credit = u
            t.save()
            reg = Registration()
            reg.program = last_program
            reg.profile = profile
            reg.personwith = bool(k)
            reg.capacity = Capacity.objects.filter(id=capid).first()
            reg.status = 'certain'
            reg.save()
            if profile.user.email:
                message2 = EmailMessage(subject='ثبت نامد در اردو' + str(last_program.title),
                                        body='شما با موفقیت در اردو ' + str(last_program.title) + 'ثبت نام شده اید ',
                                        to=[profile.user.email])
                message2.send()
        elif status == 'valid and unavailable':
            reg = Registration()
            reg.program = last_program
            reg.profile = profile
            reg.capacity = Capacity.objects.filter(id=capid).first()
            reg.status = 'reserved'
            reg.save()
            if profile.user.email:
                message2 = EmailMessage(subject='ثبت نامد در اردو' + str(last_program.title),
                                        body='شما با موفقیت در اردو برنامه   ' + str(
                                            last_program.title) + ' رزرو شده اید در صورت انصراف افراد قطعی شده برای شما یک ایمیل ارسال میشود و شما قطعی میشوید ',
                                        to=[profile.user.email])
                message2.send()
        return HttpResponseRedirect('/program/')


