# Encoding: utf-8
from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
from accounts.models import Profile
from django.db.models import Sum, Q
from django_jalali.db import models as jmodels
from django.core.mail import EmailMessage
# Create your models here.

class Program(models.Model):
    title = models.CharField(max_length=100,verbose_name='نام برنامه')
    year = models.IntegerField(null=True, blank=True,verbose_name='سال برنامه')
    isOpen = models.BooleanField(default=False,verbose_name='باز کردن ثبت نام')
    isPublic = models.BooleanField(default=False,verbose_name='نمایش برنامه ')
    programInterval = models.CharField( max_length=80,verbose_name='تاریخ برنامه به صورت ماه')
    registerInterval = models.CharField( max_length=80,verbose_name='تاریخ شروع ثبت نام')
    creationDate = models.DateTimeField(default=datetime.now,verbose_name='تاریخ ساختن برنامه')
    registerEndDate = models.DateTimeField(default=datetime.now,verbose_name='تاریخ بسته شدن ثبت نام')

    def chechEnd(self):
        from django.utils import timezone
        if timezone.now() > self.registerEndDate:
            self.isOpen = 0
            self.save()
    notes = models.CharField(max_length=200,verbose_name='یادداشت')
    startDate = models.DateField(default=datetime.now,verbose_name='تاریخ شروع برنامه')
    percentagereturnprice = models.FloatField(null=True, blank=True, default=1,verbose_name='برگشت پول در صورت انصراف')
    capecity = models.IntegerField(null=True, default=0,blank=True,verbose_name='ظرفیت کل برنامه')
    def remain(self):
        a = self.capecity
        b = Registration.objects.filter(program=self).filter(status=Registration.STATUS_CERTAIN).count()
        return (a-b)

    remain.short_description = ('مانده ظرفیت کل برنامه')
    withcapecity = models.IntegerField(null=True, default=0,blank=True,verbose_name='ظرفیت همراه')
    def withremain(self):
        a = self.withcapecity
        if not self.withcapecity:
            a = 0
        b = Registration.objects.filter(program=self).filter(status=Registration.STATUS_CERTAIN).filter(personwith=True).count()
        if not Registration.objects.filter(program=self).filter(status=Registration.STATUS_CERTAIN).filter(
                personwith=True).first():
            b = 0
        return (a-b)

    withremain.short_description = ('مانده ظرفیت همراه')
    withprice=models.IntegerField(null=True,default=0,blank=True,verbose_name='قیمت همراه')

    # capacity = models.ForeignKey(Capacity)


    class Meta:
        verbose_name = 'برنامه'
        verbose_name_plural = 'برنامه ها'


    def number_of_register(self):
        total = Registration.objects.exclude(status__contains=Registration.STATUS_REMOVED).count()
        return total

    number_of_register.short_description = ('تعداد ثبت نام شده ها')
    def certain_or_came(self):
        total = Registration.objects.filter(program=self).filter(
            Q(status=Registration.STATUS_CERTAIN) | Q(status=Registration.STATUS_CAME)).count()
        return total

    certain_or_came.short_description = ('قطعی شده یا امده')
    def __str__(self):
        return str(self.title)

    def save(self, *args, **kwargs):
        from django.utils import timezone
        g=self.registerEndDate
        u=timezone.now()
        if timezone.now() > self.registerEndDate:
            self.isOpen = 0
        # Call the "real" save()
        models.Model.save(self, *args, **kwargs)




class Capacity(models.Model):
    program = models.ForeignKey(Program, related_name='program')
    # year = Program.year
    PEOPLE_TYPE_SHARIF_STUDENT = 'sharif student'
    PEOPLE_TYPE_SHARIF_GRADUATED = 'sharif graduated'
    PEOPLE_TYPE_SHARIF_MASTER = 'sharif master'
    PEOPLE_TYPE_SHARIF_EMPLOYED = 'sharif employed'
    PEOPLE_TYPE_OTHER = 'other'
    people_type_choices = (
        (PEOPLE_TYPE_SHARIF_STUDENT, 'دانشجو شریف'),
        (PEOPLE_TYPE_SHARIF_GRADUATED, 'فارغ التحصیل شریف'),
        (PEOPLE_TYPE_SHARIF_MASTER, 'استاد شریف'),
        (PEOPLE_TYPE_SHARIF_EMPLOYED, 'کارمند شریف'),
        (PEOPLE_TYPE_OTHER, 'سایر'),
    )
    peopletype = models.CharField(max_length=200, choices=people_type_choices,verbose_name='نوع عضویت')
    # def year(self):
    #     return int(self.program.year)

    ENTERANCEYEAR_SEPARATION_ONEYEAR = 'oneyear'
    ENTERANCEYEAR_SEPARATION_TWOYEAR = 'twoyear'
    ENTERANCEYEAR_SEPARATION_THREEYEAR = 'threeyear'
    ENTERANCEYEAR_SEPARATION_FOURYEAR = 'fouryear'
    ENTERANCEYEAR_SEPARATION_FIVEYEAR = 'fiveyear'
    ENTERANCEYEAR_SEPARATION_SIXYEAR = 'sixyear'
    ENTERANCEYEAR_SEPARATION_MANYYEARSAGO = 'manyyearago'
    ENTERANCEYEAR_SEPARATION_NONE = 'none'
    enteranceyear_Separation_choices = (
        (ENTERANCEYEAR_SEPARATION_NONE, 'بدون ورودی'),
        (ENTERANCEYEAR_SEPARATION_ONEYEAR, 'سال اولی'),
        (ENTERANCEYEAR_SEPARATION_TWOYEAR, 'سال دومی'),
        (ENTERANCEYEAR_SEPARATION_THREEYEAR, 'سال سومی'),
        (ENTERANCEYEAR_SEPARATION_FOURYEAR, 'سال چهارمی'),
        (ENTERANCEYEAR_SEPARATION_FIVEYEAR, 'سال پنجمی'),
        (ENTERANCEYEAR_SEPARATION_SIXYEAR, 'سال ششمی'),
        (ENTERANCEYEAR_SEPARATION_MANYYEARSAGO, 'بیش از شش سال')
    )
    enteranceyear_Separation = models.CharField(max_length=200, choices=enteranceyear_Separation_choices, null=True,
                                                blank=True, verbose_name='تفکیک سال ورودی')
    LEVEL_TYPE_B_S = 'BS'
    LEVEL_TYPE_M_S = 'MS'
    LEVEL_TYPE_P_H_D = 'PHD'
    LEVEL_TYPE_NONE = 'none'

    level_type_choices = (
        (LEVEL_TYPE_B_S, 'کارشناسی'),
        (LEVEL_TYPE_M_S, 'کارشناسی ارشد'),
        (LEVEL_TYPE_P_H_D, 'دکترا'),
        (LEVEL_TYPE_NONE, 'بدون مقطع'),
    )
    level_Separation = models.CharField(max_length=200, choices=level_type_choices, null=True, blank=True,
                                        verbose_name='تفکیک مقطع')

    GENDER_SEPARATION_MEN = 'men'
    GENDER_SEPARATION_VOMEN = 'vomen'
    GENDER_SEPARATION_NONE = 'none'

    gender_Separation_choices = (
        (GENDER_SEPARATION_MEN, 'مرد'),
        (GENDER_SEPARATION_VOMEN, 'زن'),
        (GENDER_SEPARATION_NONE, 'مختلط'),
    )
    gender_Separation = models.CharField(max_length=200, choices=gender_Separation_choices, null=True, blank=True,
                                         verbose_name='تفکیک جنسیت')
    capecity = models.IntegerField(null=True, default=0,blank=True,verbose_name='تعداد ظرفیت')
    def remain(self):
        a = self.capecity
        b = Registration.objects.filter(capacity=self).filter(status=Registration.STATUS_CERTAIN).count()
        return (a-b)
    remain.short_description = 'مانده '
    def registrated(self):
        return Registration.objects.filter(capacity=self).filter(status=Registration.STATUS_CERTAIN).count()
    registrated.short_description = 'ثبت نام شده ها'
    price = models.IntegerField(null=True, default=0,blank=True,verbose_name='قیمت')

    def __str__(self):
        return self.get_peopletype_display() + ' ' + self.get_gender_Separation_display() + ' ' + self.get_enteranceyear_Separation_display()+' ' + self.get_level_Separation_display()


    class Meta:
        verbose_name = 'ظرفیت'
        verbose_name_plural = 'ظرفیت ها'

class Registration(models.Model):
    profile = models.ForeignKey(Profile,verbose_name='پروفایل ')
    program = models.ForeignKey(Program,verbose_name='برنامه ')
    capacity = models.ForeignKey(Capacity,verbose_name='ظرفیت ')
    registrationDate = models.DateTimeField(default=datetime.now,verbose_name='تاریخ ثبت نام ')
    feedBack = models.CharField(max_length=2000, null=True, blank=True,verbose_name='نظرات ')
    STATUS_CERTAIN = 'certain'
    STATUS_RESERVED = 'reserved'
    STATUS_GIVEN_UP = 'given up'
    STATUS_REMOVED = 'removed'
    STATUS_CAME = 'came'
    STATUS_NOT_CAME = 'not came'

    status_choices = (
        (STATUS_CERTAIN, 'قطعی'),
        (STATUS_RESERVED, 'رزرو'),
        (STATUS_GIVEN_UP, 'انصراف'),
        (STATUS_REMOVED, 'پاک شده'),
        (STATUS_CAME, 'شرکت کرده'),
        (STATUS_NOT_CAME, 'شرکت نکرده'),
    )
    status = models.CharField(max_length=200, choices=status_choices,verbose_name='وضعیت ')
    personwith = models.BooleanField(default=0,verbose_name='همراه ')

    def reserved(self):
        if self.status == 'certain':
            program = self.program
            profile = self.profile
            capacity = self.capacity
            withbackprice = 0
            if self.personwith:
                withbackprice = program.withprice
                program.save()
            self.status = Registration.STATUS_GIVEN_UP
            self.save()
            profile.credit = profile.credit + ((program.percentagereturnprice) * int(capacity.price + withbackprice))
            profile.save()
            u = Registration.objects.filter(capacity=capacity).filter(status='reserved')
            program = capacity.program
            if u:
                if program.isOpen:
                    date_list = []
                    for item in u:
                        date_list.append(item.id)
                    r = min(date_list)
                    if r:
                        reg = Registration.objects.get(id=r)
                        profile = reg.profile
                        credits = profile.credit
                        capacity = capacity
                        u1 = credits - capacity.price
                        profile.credit = u1
                        profile.save()
                        reg.status = 'certain'
                        reg.capacity = capacity
                        reg.save()
                        if profile.user.email:
                            message2 = EmailMessage(subject='ثبت نامد در اردو' + str(program.title),
                                                    body='شما با موفقیت در اردو ' + str(
                                                        program.title) + 'ثبت نام شده اید ', to=[profile.user.email])
                            message2.send()

        elif self.status == 'reserved':
            self.status = Registration.STATUS_GIVEN_UP
            self.save()
    class Meta:
        verbose_name = 'ثبت نام'
        verbose_name_plural = 'ثبت نام ها'
    def __str__(self):
        return self.program.title + ' ' + self.profile.user.get_full_name()




class Message(models.Model):
    sender = models.ForeignKey(Profile,verbose_name='پروفایل ')
    STATUS_PROFILE = 'profile'
    STATUS_REGISTRATION = 'Registration'


    status_choices = (
        (STATUS_PROFILE, 'پروفایل'),
        (STATUS_REGISTRATION, 'ثبت نام'),

    )
    status = models.CharField(max_length=200, choices=status_choices,verbose_name='وضیعت ')
    subject = models.CharField(max_length=200, null=True, blank=True,verbose_name='موضوع ')
    content = models.CharField(max_length=1000, null=True, blank=True,verbose_name='متن ')
    sendEmail = models.BooleanField(default=False,verbose_name='ایمیل ')
    sendSms = models.BooleanField(default=False,verbose_name='sms ')
    sendInbox = models.BooleanField(default=False,verbose_name='داخل برنامه ')
    messageSendDate = jmodels.jDateTimeField(default=datetime.now)

    class Meta:
        verbose_name = 'پیام'
        verbose_name_plural = 'پیام ها'

    def __str__(self):
        return str(self.sender)


class Message_reciving(models.Model):
    message = models.ForeignKey(Message,verbose_name='پیام ')
    registration = models.ForeignKey(Registration,null=True,verbose_name='ثبت نام ')
    profile=models.ForeignKey(Profile,null=True,verbose_name='پروفایل ')

    # def __str__(self):
    #     return self.message

    class Meta:
        verbose_name = 'دریافت پیام'
        verbose_name_plural = 'دریافت پیام ها'
